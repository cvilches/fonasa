from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import TemplateView
from rest_framework import viewsets
from rest_framework.decorators import api_view, action

from saap.models import (Hospital, Consulta, Paciente, PacienteNino, PacienteJoven, PacienteAnciano, AtencionPaciente)
from saap.serializers import (HospitalSerializer, ConsultaSerializer, PacienteSerializer, PacienteNinoSerializer,
                              PacienteJovenSerializer, PacienteAncianoSerializer, AtencionPacienteSerializer)
from saap.libs.atenciones import optimizar_atencion, liberar_consultas


class IndexPageView(TemplateView):
    def get(self, request, **kwargs):
        return render(request, 'index.html', context=None)


class HospitalView(viewsets.ModelViewSet):
    queryset = Hospital.objects.filter(visible=True)
    serializer_class = HospitalSerializer


class ConsultaView(viewsets.ModelViewSet):
    queryset = Consulta.objects.all()
    serializer_class = ConsultaSerializer

    def get_queryset(self):
        return Consulta.objects.filter(hospital=self.kwargs['hospital'])

    @action(methods=['get'], detail=False, url_path='optimizarConsultas', url_name='optimizarConsultas')
    def optimizar_atencion(self, request, hospital=None):
        """
        Esta funcion optimiza las atenciones
        :param request:
        :param hospital:
        :return:
        """
        optimizar_atencion(hospital)
        return JsonResponse({"status": "ok"}, safe=False)

    @action(methods=['get'], detail=False, url_path='liberarConsultas', url_name='liberarConsultas')
    def liberar_consultas(self, request, hospital=None):
        """
        Funcion libera las consultas y atiende a los pacientes de la sala de espera
        :param request:
        :param hospital:
        :return:
        """
        liberar_consultas(hospital)
        return JsonResponse({"status": "ok"}, safe=False)

    @action(methods=['get'], detail=False, url_path='masAtenciones', url_name='masAtenciones')
    def mas_pacientes_atendidos(self, request, hospital=None):
        """
        Esta funcion obtiene consulta con mas atenciones
        :param request:
        :param hospital:
        :return:
        """
        data = Consulta.objects.filter(hospital=hospital).order_by('-cantidad_pacientes').first()
        return JsonResponse(AtencionPacienteSerializer(data).data, safe=False)


class PacienteView(viewsets.ModelViewSet):
    queryset = Paciente.objects.all().order_by('-prioridad')

    @action(methods=['get'], detail=True, url_path='atender', url_name='atender')
    def atender(self, request, pk=None, hospital=None):
        """
        Esta funcióm agrega un paciente a una atención, gregistrando la hora de ingreso
        ejemplo url:http://localhost:8000/api/1/paciente/2/atender/
        :param request: request
        :param pk: id paciente (no se usa pero es requerido para acceder a la funcion)
        :param hospital: id hospital
        :return: el pacinete ingresado
        """
        AtencionPaciente.objects.create(paciente_id=int(pk))
        Paciente.objects.filter(id=pk).update(estado='pendiente')
        data = Paciente.objects.get(id=pk)
        return JsonResponse(PacienteSerializer(data).data, safe=False)

    @action(methods=['get'], detail=False, url_path='pacientesMayorRiesgo', url_name='pacientesMayorRiesgo')
    def listar_pacientes_mayor_riesgo(self, request, hospital=None):
        """
        Esta funcion lista pacientes de mayor riesgo segun orden de prioridad
        ejemplo url:http://localhost:8000/api/1/paciente/pacientesMayorRiesgo/
        :param request:
        :param hospital: id hospital
        :return: lista de pacientes json
        """
        data = list(Paciente.objects.filter(hospital=hospital).order_by('-prioridad').values())
        return JsonResponse(data, safe=False)

    def get_queryset(self):
        return Paciente.objects.filter(hospital=self.kwargs['hospital'])

    serializer_class = PacienteSerializer


class PacienteNinoView(viewsets.ModelViewSet):
    queryset = PacienteNino.objects.all()
    serializer_class = PacienteNinoSerializer

    def get_queryset(self):
        return PacienteNino.objects.filter(hospital=self.kwargs['hospital'])


class PacienteJovenView(viewsets.ModelViewSet):
    queryset = PacienteJoven.objects.all()
    serializer_class = PacienteJovenSerializer

    def get_queryset(self):
        return PacienteJoven.objects.filter(hospital=self.kwargs['hospital'])

    @action(methods=['get'], detail=False, url_path='fumadoresUrgentes', url_name='fumadoresUrgentes')
    def fumadores_urgentes(self, request, hospital=None):
        """
        Funcion obtiene pacientes fumadores urgentes
        :param request:
        :param hospital:
        :return:
        """
        qs = PacienteJoven.objects.filter(hospital=hospital, es_fumador=True).order_by('-prioridad').values()
        data = list(qs)
        return JsonResponse(data, safe=False)


class PacienteAncianoView(viewsets.ModelViewSet):
    queryset = PacienteAnciano.objects.all()
    serializer_class = PacienteAncianoSerializer

    def get_queryset(self):
        return PacienteAnciano.objects.filter(hospital=self.kwargs['hospital'])

    @action(methods=['get'], detail=False, url_path='fumadoresUrgentes', url_name='fumadoresUrgentes')
    def fumadores_urgentes(self, request, hospital=None):
        """
        Funcion obtiene paciente mas anciano
        :param request:
        :param hospital:
        :return:
        """
        data = PacienteAnciano.objects.filter(hospital=hospital).order_by('-edad').first()
        return JsonResponse(AtencionPacienteSerializer(data).data, safe=False)


class AtencionPacienteView(viewsets.ModelViewSet):
    queryset = AtencionPaciente.objects.filter(activa=True)
    serializer_class = AtencionPacienteSerializer

    @action(methods=['POST'], detail=False, url_path='nuevaAtencion', url_name='nuevaAtencion')
    def nueva_atencion(self, request, hospital=None):
        """
        Esta funcion crea un atencion si  no existe.
        si existe retorna la que existe

        :param request:
        :param hospital:
        :param paciente:
        :return:
        """
        pacientes = request.data['pacientes']
        for p in pacientes:


            at = AtencionPaciente.objects.filter(paciente=p['id'], activa=True)
            if len(at) == 0:
                at = AtencionPaciente(paciente_id=p['id'], activa=True)
                at.paciente.estado = 'pendiente'
                at.paciente.save()
                at.save()
        return JsonResponse({"consultas actualizadas": "ok"}, safe=False)

    @action(methods=['POST'], detail=False, url_path='cancelarAtencion', url_name='cancelarAtencion')
    def cancelar_atencion(self, request, hospital=None):
        """
        Esta funcion crea un atencion si  no existe.
        si existe retorna la que existe

        :param request:
        :param hospital:
        :param paciente:
        :return:
        """
        pacientes = request.data['pacientes']
        atenciones = AtencionPaciente.objects.filter(paciente__in=pacientes)

        for a in atenciones:
            a.activa = False
            a.paciente.estado = 'ingresado'
            if a.consulta:
                a.consulta.estado = 'espera'
                a.consulta.save()
            a.paciente.save()
            a.save()
        return JsonResponse({"consultas canceladas": "ok"}, safe=False)


@api_view(['GET', ])
def pacientes(request, hospital):
    pn = PacienteNino.objects.all()
    pa = PacienteAnciano.objects.all()
    pj = PacienteJoven.objects.all()
    p = list(pn.values()) + list(pa.values()) + list(pj.values())

    return JsonResponse(p, safe=False)
