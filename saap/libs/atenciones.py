from saap.models import AtencionPaciente, Consulta, Paciente, Hospital


def atencion_masiva(atenciones, tipo, hospital):
    for pu in atenciones:
        consulta = Consulta.objects.filter(tipo=tipo, estado='espera', hospital=hospital).first()
        if consulta:

            pu.consulta = consulta
            consulta.cantidad_pacientes += 1
            consulta.estado = 'ocupado'
            pu.paciente.estado = 'en_consulta'
            pu.save()
            pu.paciente.save()
            consulta.save()


def optimizar_atencion(hospital):
    """Esta funcion optimiza las atenciones """

    pacientes_urgentes = AtencionPaciente.objects.filter(
        paciente__estado__in=['pendiente', 'espera'], paciente__hospital=hospital,
        paciente__prioridad__gt=4, activa=True
    ).order_by('-paciente__prioridad')

    pacientes_pediatria = AtencionPaciente.objects.filter(
        paciente__estado__in=['pendiente', 'espera'],
        paciente__tipo_paciente='nino', activa=True, paciente__hospital=hospital, paciente__prioridad__lte=4
    ).order_by('fecha_ingreso')
    pacientes_cgi = AtencionPaciente.objects.filter(
        paciente__estado__in=['pendiente', 'espera'], paciente__hospital=hospital, paciente__prioridad__lte=4,
        paciente__tipo_paciente__in=['anciano', 'joven'], activa=True
    ).order_by('fecha_ingreso')


    # asignar pacientes a consultas
    atencion_masiva(pacientes_urgentes, 'urgencia', hospital)
    atencion_masiva(pacientes_pediatria, 'pediatria', hospital)
    atencion_masiva(pacientes_cgi, 'cgi', hospital)

    # pacientes a sala de espera

    cse = Hospital.objects.get(id=hospital).cap_sala_espera
    pendientes = AtencionPaciente.objects.filter(paciente__estado='pendiente', activa=True).order_by('-fecha_ingreso')[
                 :cse]
    for p in pendientes:
        p.paciente.estado = 'espera'
        p.paciente.save()


def liberar_consultas(hospital):
    """ en esta funcion liberamos las consultas y atendemos a los pacientes de la sala de espera"""
    atenciones = AtencionPaciente.objects.filter(paciente__hospital=hospital, paciente__estado='en_consulta',
                                                 activa=True)

    for a in atenciones:
        # if a.consulta:
        a.consulta.estado = 'espera'
        a.consulta.save()
        a.paciente.estado = 'atendido'
        a.activa = False
        a.paciente.save()
        a.save()

    # se atiende solo a pacientes de sala de espera
    pacientes_urgentes = AtencionPaciente.objects.filter(
        paciente__estado='espera', paciente__prioridad__gt=4, paciente__hospital=hospital, activa=True
    ).order_by('-paciente__prioridad')

    pacientes_pediatria = AtencionPaciente.objects.filter(
        paciente__estado='espera', paciente__tipo_paciente='nino', paciente__hospital=hospital,
        paciente__prioridad__lte=4, activa=True).order_by('fecha_ingreso')
    pacientes_cgi = AtencionPaciente.objects.filter(
        paciente__estado='espera', paciente__tipo_paciente__in=['anciano', 'joven'], paciente__hospital=hospital,
        paciente__prioridad__lte=4, activa=True).order_by('fecha_ingreso')

    # asignar pacientes a consultas
    atencion_masiva(pacientes_urgentes, 'urgencia', hospital)
    atencion_masiva(pacientes_pediatria, 'pediatria', hospital)
    atencion_masiva(pacientes_cgi, 'cgi', hospital)
