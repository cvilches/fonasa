def obtener_prioridad(paciente):
    """
    Esta función calcula la prioridad del paciente
    :param paciente:
    :return prioridad:
    """

    edad = paciente.edad

    prioridad = 0
    adicional_anciano = 0
    if 0 <= edad <= 5:
        prioridad = paciente.peso_estatura + 3
    elif 6 <= edad <= 12:
        prioridad = paciente.peso_estatura + 2
    elif 13 <= edad <= 15:
        prioridad = paciente.peso_estatura + 1
    elif 16 <= edad <= 40:
        if paciente.es_fumador:
            prioridad = paciente.tiempo / 4 + 2
        else:
            prioridad = 2
    elif 41 < edad:
        adicional_anciano = 5.3
        if 60 <= edad <= 100:
            prioridad = edad / 20 + 4
        else:
            prioridad = edad / 30 + 3
    # todo como se determina
    return round((((edad * prioridad) / 100) + adicional_anciano), 2)


def tipo_paciente(paciente):
    """
    Esta función retorna el tipo de paciente biño joven o adulto segun su edad
    :param paciente:
    :return tipo_paciente:
    """
    edad = paciente.edad
    if 0 <= edad <= 15:
        tipo = 'nino'
    elif 16 <= edad <= 40:
        tipo = 'joven'
    elif 41 <= edad:
        tipo = 'anciano'
    else:
        tipo = ''
    return tipo

