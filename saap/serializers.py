from rest_framework import serializers
from saap.models import Hospital, Consulta, Paciente, PacienteNino, PacienteJoven, PacienteAnciano, AtencionPaciente


class ConsultaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consulta
        fields = '__all__'


class HospitalSerializer(serializers.ModelSerializer):
    # consultas = ConsultaSerializer(many=True, read_only=True)

    class Meta:
        model = Hospital
        exclude = []

class PacienteNinoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PacienteNino
        fields = ['nombre', 'edad', 'numero_historia_clinica', 'tipo_paciente', 'tipo_paciente', 'peso_estatura']


class PacienteJovenSerializer(serializers.ModelSerializer):
    class Meta:
        model = PacienteJoven
        fields = '__all__'


class PacienteAncianoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PacienteAnciano
        fields = '__all__'


class PacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paciente
        fields = '__all__'

class AtencionPacienteSerializer(serializers.ModelSerializer):
    paciente = PacienteSerializer()
    consulta = ConsultaSerializer()
    class Meta:
        # depth = 1
        model = AtencionPaciente
        fields = '__all__'

