from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from saap.libs.utils import obtener_prioridad, tipo_paciente

CHOICE_TIPO_CONSULTA = (
    ('pediatria', 'Pediatría'),
    ('urgencia', 'Urgencias'),
    ('cgi', 'Consulta General Integral')
)

CHOICE_ESTADO_CONSULTA = (
    ('ocupado', 'Ocupado'),
    ('espera', 'En espera')

)

CHOICE_TIPO_PACIENTE = (
    ('nino', 'Niño'),
    ('joven', 'Joven'),
    ('anciano', 'Anciano')
)

CHOICE_ATENCION = (
    ('ingresado', 'Ingresado'),
    ('pendiente', 'Pendiente'),
    ('espera', 'En sala de espera'),
    ('en_consulta', 'En consulta'),
    ('atendido', 'Atendido')
)


class Hospital(models.Model):
    nombre = models.CharField(max_length=30)
    visible = models.BooleanField(default=False)
    cap_sala_espera = models.IntegerField(default=2, verbose_name='Capacidad sala de espera')

    class Meta:
        verbose_name = 'Hospital'
        verbose_name_plural = 'Hospitales'

    def __str__(self):
        return self.nombre


class Consulta(models.Model):
    cantidad_pacientes = models.IntegerField(default=0)
    nombre_especialista = models.CharField(max_length=50)
    tipo = models.CharField(max_length=20, choices=CHOICE_TIPO_CONSULTA)
    estado = models.CharField(default='espera', max_length=20, choices=CHOICE_ESTADO_CONSULTA)
    hospital = models.ForeignKey(Hospital, on_delete=models.CASCADE, related_name="consultas")

    def __str__(self):
        return self.nombre_especialista


class Paciente(models.Model):
    hospital = models.ForeignKey(Hospital, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30)
    estado = models.CharField(max_length=30, choices=CHOICE_ATENCION, default='ingresado')
    numero_historia_clinica = models.IntegerField(unique=True)
    tipo_paciente = models.CharField(max_length=10, choices=CHOICE_TIPO_PACIENTE)
    prioridad = models.FloatField(default=0)

    def __str__(self):
        return self.nombre


class PacienteAnciano(Paciente):
    edad = models.IntegerField(default=0, validators=[MinValueValidator(41)])
    tiene_dieta = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.tipo_paciente = tipo_paciente(self)
        self.prioridad = obtener_prioridad(self)
        super().save(*args, **kwargs)


class PacienteJoven(Paciente):
    edad = models.IntegerField(default=0, validators=[MinValueValidator(16), MaxValueValidator(40)])
    es_fumador = models.BooleanField(default=False)
    tiempo = models.IntegerField(default=0, help_text='Años de fumador')

    def save(self, *args, **kwargs):
        self.tipo_paciente = tipo_paciente(self)
        self.prioridad = obtener_prioridad(self)
        super().save(*args, **kwargs)


class PacienteNino(Paciente):
    edad = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(15)])
    peso_estatura = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(4)])

    def save(self, *args, **kwargs):
        self.tipo_paciente = tipo_paciente(self)
        self.prioridad = obtener_prioridad(self)
        super().save(*args, **kwargs)


class AtencionPaciente(models.Model):
    consulta = models.ForeignKey(Consulta, on_delete=models.SET_NULL, null=True, related_name='atenciones')
    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE, related_name='atenciones')
    fecha_ingreso = models.DateTimeField(auto_now_add=True)
    activa = models.BooleanField(default=True)
