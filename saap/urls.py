from django.conf.urls import url

from saap import views

urlpatterns = [
    url(r'^', views.IndexPageView.as_view()),
]
