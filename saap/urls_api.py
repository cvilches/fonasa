from django.conf.urls import url

from saap import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'hospital', views.HospitalView)
router.register(r'(?P<hospital>[0-9]+)/consulta', views.ConsultaView)
router.register(r'^(?P<hospital>[0-9]+)/paciente', views.PacienteView)
# router.register(r'^(?P<hospital>[0-9]+)/paciente/{pk}/atender/$', views.PacienteView)
router.register(r'(?P<hospital>[0-9]+)/paciente-nino', views.PacienteNinoView)
router.register(r'(?P<hospital>[0-9]+)/paciente-joven', views.PacienteJovenView)
router.register(r'(?P<hospital>[0-9]+)/paciente-anciano', views.PacienteAncianoView)
router.register(r'(?P<hospital>[0-9]+)/atencion', views.AtencionPacienteView)

urlpatterns = [
    url(r'^(?P<hospital>[0-9])/pacientes/$', views.pacientes),
]

urlpatterns += router.urls
