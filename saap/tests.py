from django.test import TestCase
from saap.models import PacienteAnciano, PacienteNino, PacienteJoven


class PacientesTestCase(TestCase):
    """Este set de pruebas valida el tipo de paciente y la prioridad"""

    def setUp(self):
        PacienteNino.objects.create(nombre='Aurora Coihuan', edad=2, numero_historia_clinica=3, peso_estatura=4)
        PacienteNino.objects.create(nombre='Andrea Molina', edad=7, numero_historia_clinica=6, peso_estatura=2)
        PacienteNino.objects.create(nombre='Eduardo Vilches', edad=14, numero_historia_clinica=5, peso_estatura=3)
        PacienteJoven.objects.create(nombre='Eduardo Corona', edad=35, numero_historia_clinica=4, es_fumador=True,
                                     tiempo=5)
        PacienteJoven.objects.create(nombre='María Calderon', edad=22, numero_historia_clinica=7, es_fumador=False)
        PacienteAnciano.objects.create(nombre='Joel Manquel', edad=42, numero_historia_clinica=1, tiene_dieta=True)
        PacienteAnciano.objects.create(nombre='Jorge Vega', edad=70, numero_historia_clinica=2, tiene_dieta=True)

    def test_model_paciente_nino(self):
        """Prueba la prioridad
        paciente Niño"""
        p1 = PacienteNino.objects.get(nombre="Aurora Coihuan")
        p2 = PacienteNino.objects.get(nombre="Andrea Molina")
        p3 = PacienteNino.objects.get(nombre="Eduardo Vilches")

        self.assertEqual(p1.prioridad, 0.14)
        self.assertEqual(p2.prioridad, 0.28)
        self.assertEqual(p3.prioridad, 0.56)
        self.assertEqual(p1.tipo_paciente, 'Nino')

    def test_model_paciente_joven(self):
        """Prueba la prioridad paciente Niño"""
        p1 = PacienteJoven.objects.get(nombre="Eduardo Corona")
        p2 = PacienteJoven.objects.get(nombre="María Calderon")

        self.assertEqual(p1.prioridad, 1.14)
        self.assertEqual(p2.prioridad, 0.44)
        self.assertEqual(p1.tipo_paciente, 'Joven')

    def test_model_paciente_anciano(self):
        """Prueba la prioridad paciente Anciano"""
        p1 = PacienteAnciano.objects.get(nombre="Joel Manquel")
        p2 = PacienteAnciano.objects.get(nombre="Jorge Vega")

        self.assertEqual(p1.prioridad, 7.15)
        self.assertEqual(p2.prioridad, 10.55)
        self.assertEqual(p1.tipo_paciente, 'Anciano')
