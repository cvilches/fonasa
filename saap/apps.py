from django.apps import AppConfig


class SaapConfig(AppConfig):
    name = 'saap'
