from django.contrib import admin
from saap.models import Hospital, Consulta, PacienteNino, PacienteJoven, PacienteAnciano, AtencionPaciente

admin.site.site_header = 'FONASA - SaaP'

"""
Se usa django admin para los mantenedores
Documentacion de clases y metodos en https://docs.djangoproject.com/en/2.2/ref/contrib/admin/
"""


class ConsultaAdmin(admin.TabularInline):
    fields = ('nombre_especialista', 'tipo')
    model = Consulta
    extra = 1


class HospitalAdmin(admin.ModelAdmin):
    inlines = [ConsultaAdmin]


class PacienteAdmin(admin.ModelAdmin):
    exclude = ('prioridad', 'tipo_paciente',)
    list_filter = ('hospital',)


@admin.register(PacienteNino)
class PacienteNinoAdmin(PacienteAdmin):
    list_display = (
        'hospital', 'estado', 'nombre', 'numero_historia_clinica', 'edad', 'prioridad', 'tipo_paciente',
        'peso_estatura')


@admin.register(PacienteJoven)
class PacienteJovenAdmin(PacienteAdmin):
    list_display = (
        'hospital', 'estado', 'nombre', 'numero_historia_clinica', 'edad', 'prioridad', 'tipo_paciente', 'es_fumador',
        'tiempo')


@admin.register(PacienteAnciano)
class PacienteAncianoAdmin(PacienteAdmin):
    list_display = (
        'hospital', 'estado', 'nombre', 'numero_historia_clinica', 'tiene_dieta', 'edad', 'prioridad', 'tipo_paciente')


@admin.register(AtencionPaciente)
class AtencionAdmin(admin.ModelAdmin):
    exclude = ('consulta',)
    list_display = ('hospital', 'paciente', 'prioridad', 'estado', 'consulta',)
    list_filter = ('paciente__hospital', 'paciente__estado',)

    def prioridad(self, obj):
        return obj.paciente.prioridad

    def estado(self, obj):
        return obj.paciente.estado

    def hospital(self, obj):
        return obj.paciente.hospital


admin.site.register(Hospital, HospitalAdmin)
