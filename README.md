# Saap - Fonasa
Sistema de atención automatizada al paciente. SAAP Proyecto enmarcado en el proceso de Transformación Digital para hospitales de la Región Metropolitana.

## Caracteristicas

* Agregar pacientes diferenciados por rango de edad
* Listar pacientes de mayor riesgo
* Listar pacientes fumadores urgentes
* Ver al paciente mas anciano
* Optimizar las atenciones segun urgencia y orden de llegada

## Dependencias 

| Nombre         | Version     |
| ---------------| ----------- |
| Python         | 3.7         |
| Django         | 2.2.7       |
| DRF            | 3.10.3      |
| Angular        | 8.2.14      |
| SQlite         | 3.29.0      |
| Docker         | 19.03.3     |
| docker-compose | 1.24.1      |

En ambientes productivos se recomienda Postgres o MariaDB   

## Correr programa

#### Instalar recursos

[Instalar Docker](https://docs.docker.com/install/)

[Instalar docker-compose](https://docs.docker.com/compose/install/)



#### Levantar aplicación

Levantar docker-compose en la ruta de la aplicación.
El frontend hecho en agular ya se encuentra compilado.

```shell script
cd PATH/saap/docker/
docker-compose up -d
```
URLS de la apalicación
* FrontEnd: `http://localhost:8080`
* Administrador: `http://localhost:8000` usuario `admin` contraseña `admin`
* API: `http://localhost:8000/api/`

