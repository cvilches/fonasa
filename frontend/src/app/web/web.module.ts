import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import { routes } from './routes';

@NgModule({
  declarations: [],
  imports: [
        SharedModule,
        RouterModule.forRoot(routes)
  ],
    exports: [
        RouterModule
    ]
})
export class WebModule { }
