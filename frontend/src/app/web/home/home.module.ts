import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import { PacientesComponent } from './home/pacientes/pacientes.component';
import { ConsultasComponent } from './home/consultas/consultas.component';
import { AtencionesComponent } from './home/atenciones/atenciones.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
];

@NgModule({
    imports: [
      SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [HomeComponent, PacientesComponent, ConsultasComponent, AtencionesComponent],
    exports: [
        RouterModule
    ]
})
export class HomeModule { }
