import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConsultasService {

  constructor(private http: HttpClient) {
  }

  baseUrl = 'http://localhost:8000/api/';

  getHospitales(): Observable<any> {
    return this.http.get<any>(this.baseUrl + 'hospital/');
  }

  getPacientes(id: string): Observable<any> {
    return this.http.get<any>(this.baseUrl + id + '/pacientes/');
  }

  getConsultas(id: string): Observable<any> {
    return this.http.get<any>(this.baseUrl + id + '/consulta/');
  }


  getAtenciones(id: string): Observable<any> {
    return this.http.get<any>(this.baseUrl + id + '/atencion/');
  }

  atenderPaciente(hospital, pacientes): Observable<any> {
    const url = this.baseUrl + hospital + '/atencion/nuevaAtencion/';
    console.log(url);
    const body = {active: 1, pacientes};
    return this.http.post(url, body, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  cancelarPaciente(hospital, pacientes): Observable<any> {
    const url = this.baseUrl + hospital + '/atencion/cancelarAtencion/';
    console.log(url);
    const body = {active: 1, pacientes};
    return this.http.post(url, body, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  liberarConsultas(hospital): Observable<any> {
    return this.http.get<any>(this.baseUrl + hospital + '/consulta/liberarConsultas/');
  }

  optiomizarConsultas(hospital): Observable<any> {
    return this.http.get<any>(this.baseUrl + hospital + '/consulta/optimizarConsultas/');
  }


}
