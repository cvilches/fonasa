import {Component, OnInit} from '@angular/core';
import {ConsultasService} from '../consultas.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private cs: ConsultasService) {
  }

  hospitales = [];
  pacientes = [];
  atenciones = [];
  hospital = {
    consultas: undefined,
    id: undefined,
    nombre: undefined
  };

  ngOnInit() {

    this.getData();
  }


  getData() {
    this.cs.getHospitales().subscribe(
      data => this.hospitales = data,
      error => console.log(error),
      () => this.hospital = this.hospitales[0]
    );


  }

  getPaciente(id) {
    const paciente = this.pacientes.filter(i => i.id = id)[0];
    return paciente.numero_historia_clinica + ' ' + paciente.nombre
  }


}
