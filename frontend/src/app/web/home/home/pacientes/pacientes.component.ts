import {Component, OnInit, Input} from '@angular/core';
import {ConsultasService} from '../../consultas.service';


@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.scss']
})
export class PacientesComponent implements OnInit {

  @Input('hospital') hospital: string;

  constructor(private cs: ConsultasService) {
  }

  pacientes = [];
  // clon de pacientes
  bkPacientes = [];

  selected = [];

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.cs.getPacientes(this.hospital).subscribe(
      data => this.pacientes = data,
      error => console.log(error),
      () => this.bkPacientes = (JSON.parse(JSON.stringify(this.pacientes)))
    );
  }

  atenterSeleccion() {
    this.cs.atenderPaciente(this.hospital, this.selected).subscribe(
      data => console.log(data),
      err => console.log(err),
      () => this.getData()
    );
  }

  cancelarSeleccion() {
    const pacientes = this.selected.map(i => i.id);
    this.cs.cancelarPaciente(this.hospital, pacientes).subscribe(
      data => console.log(data),
      err => console.log(err),
      () => this.getData()
    );
  }

  pacintesMayorRiesgo() {
    console.log('pacintesMayorRiesgo');
    this.pacientes = this.pacientes.sort((a, b) => b.prioridad - a.prioridad);
  }

  fumadoresUrgentes() {
    this.pacientes = this.pacientes
      .filter(i => i.tipo = 'joven')
      .filter(i => i.es_fumador && i.prioridad > 4)
      .sort((a, b) => b.prioridad - a.prioridad);
    console.log('fumadoresUrgentes');
  }

  masAnciano() {
    this.pacientes = (JSON.parse(JSON.stringify(this.bkPacientes)));
    this.pacientes = this.pacientes.sort((a, b) => b.edad - a.edad);
    console.log('masAnciano');
  }

  pacintesTodos() {
    this.pacientes = (JSON.parse(JSON.stringify(this.bkPacientes)));
  }
}
