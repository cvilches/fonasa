import {Component, Input, OnInit} from '@angular/core';
import {ConsultasService} from '../../consultas.service';

@Component({
  selector: 'app-atenciones',
  templateUrl: './atenciones.component.html',
  styleUrls: ['./atenciones.component.scss']
})
export class AtencionesComponent implements OnInit {

  @Input('hospital') hospital: string;
  atenciones = [];
  selected = [];

  constructor(private cs: ConsultasService) {

  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.cs.getAtenciones(this.hospital).subscribe(
      data => this.atenciones = data,
      error => console.log(error),
      () => console.log(this.atenciones)
    );
  }
}
