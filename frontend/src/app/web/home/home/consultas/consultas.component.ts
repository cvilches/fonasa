import {Component, Input, OnInit} from '@angular/core';
import {ConsultasService} from '../../consultas.service';

@Component({
  selector: 'app-consultas',
  templateUrl: './consultas.component.html',
  styleUrls: ['./consultas.component.scss']
})
export class ConsultasComponent implements OnInit {

  @Input('hospital') hospital: string;
  consultas = [];
  selected = [];

  constructor(private cs: ConsultasService) {
  }

  ngOnInit() {
    // esperamos que esté dispobible el id del hospital
    setTimeout(() => this.getData(), 400);
  }

  getData() {
    this.cs.getConsultas(this.hospital).subscribe(
      data => this.consultas = data,
      error => console.log(error),
      () => console.log(this.consultas)
    );
  }

  liberar() {
    console.log('liberar');
    this.cs.liberarConsultas(this.hospital).subscribe(
      data => console.log(data),
      error => console.log(error),
      () => this.getData()
    );
  }

  optimizar() {
    console.log('optinizar');
    this.cs.optiomizarConsultas(this.hospital).subscribe(
      data => console.log(data),
      error => console.log(error),
      () => this.getData()
    );

  }

}
